package ru.tsc.chertkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.task.TaskShowListByProjectIdRequest;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = getServiceLocator().getTaskEndpoint()
                .showListByProjectIdTask(new TaskShowListByProjectIdRequest(getToken(), projectId))
                .getTasks();
        renderTasks(tasks);
    }

}
