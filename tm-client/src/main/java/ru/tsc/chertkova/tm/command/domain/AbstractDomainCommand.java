package ru.tsc.chertkova.tm.command.domain;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.command.AbstractCommand;
import ru.tsc.chertkova.tm.dto.Domain;

@NoArgsConstructor
public abstract class AbstractDomainCommand extends AbstractCommand {
}
