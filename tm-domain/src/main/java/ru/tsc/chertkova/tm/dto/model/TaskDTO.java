package ru.tsc.chertkova.tm.dto.model;

import lombok.*;
import org.jetbrains.annotations.*;
import ru.tsc.chertkova.tm.dto.model.AbstractUserOwnerModelDTO;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.util.DateUtil;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "TM_TASK")
public final class TaskDTO extends AbstractUserOwnerModelDTO {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "NAME")
    private String name = "";

    @NotNull
    @Column(name = "DESCRIPTION")
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "PROJECT_ID")
    private String projectId;

    @NotNull
    @Column(name = "CREATED_DT")
    private Date created = new Date();

    @Nullable
    @Column(name = "STARTED_DT")
    private Date dateBegin;

    @Nullable
    @Column(name = "COMPLETED_DT")
    private Date dateEnd;

    public TaskDTO(@NotNull final String name,
                   @NotNull final Status status,
                   @Nullable final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public TaskDTO(@NotNull final String name,
                   @NotNull final String description,
                   @Nullable final String projectId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return getId() + " - " + name + " : " +
                description + ", " + status + ", " + DateUtil.toString(dateBegin);
    }

}
