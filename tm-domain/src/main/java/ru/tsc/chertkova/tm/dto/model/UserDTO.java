package ru.tsc.chertkova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "TM_USER")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "EMAIL")
    private String email;

    @Nullable
    @Column(name = "FIRST_NAME")
    private String firstName;

    @Nullable
    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "LOCKED")
    private boolean locked = false;

    @NotNull
    @Column(name = "LOGIN")
    private String login;

    @Nullable
    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @NotNull
    @Column(name = "PASSWORD")
    private String passwordHash;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE")
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "CREATED")
    private Date created = new Date();

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash,
                   @NotNull final String email, @Nullable final String firstName,
                   @Nullable final String middleName, @Nullable final String lastName) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.locked = false;
    }

    @Override
    public String toString() {
        return "Id " + getId() +
                " email='" + email +
                ", firstName='" + firstName +
                ", lastName='" + lastName +
                ", middleName='" + middleName +
                ", role=" + role;
    }

}
