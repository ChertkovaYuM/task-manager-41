package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Session;
import ru.tsc.chertkova.tm.model.User;

public interface IAuthService {

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

}
