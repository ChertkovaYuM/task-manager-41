package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.dto.model.ProjectDTO;
import ru.tsc.chertkova.tm.dto.model.TaskDTO;
import ru.tsc.chertkova.tm.dto.model.UserDTO;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IPropertyService propertyService;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        sqlSessionFactory = getSqlSessionFactory();
        entityManagerFactory = factory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getSqlSession() {
        return getSqlSessionFactory().openSession();
    }

    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String user = propertyService.getDatabaseUsername();
        @Nullable final String password = propertyService.getDatabasePassword();
        @Nullable final String url = propertyService.getDatabaseUrl();
        @Nullable final String driver = propertyService.getDatabaseDriver();
        final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    @Override
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getDatabaseUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDatabaseHBM2DDL());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDatabaseShowSQL());
        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
